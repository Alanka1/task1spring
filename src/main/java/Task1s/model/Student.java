package Task1s.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "student")
public class Student {
    @Id
    private int id;
    private String name;
    private String phone;
    private int groupid;
}