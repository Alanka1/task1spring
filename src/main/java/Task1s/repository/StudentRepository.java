package Task1s.repository;

import Task1s.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository
        extends CrudRepository<Student, Long> {
    @Query(value = "SELECT * FROM student WHERE groupid = 1", nativeQuery = true)
    public List<Student> getStudentGroup1();
    List<Student> findAllByGroupid(int groupid);

}


