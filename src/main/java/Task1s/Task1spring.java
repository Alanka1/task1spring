package Task1s;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task1spring {

	public static void main(String[] args) {
		SpringApplication.run(Task1spring.class, args);
	}

}
